# MERN-Motoid Starter

This is a boilerplate project using the following technologies:

- [React](https://facebook.github.io/react/) and [React Router](https://reacttraining.com/react-router/) for the frontend
- [Express](http://expressjs.com/) and [Mongoose](http://mongoosejs.com/) for the backend
- [Sass](http://sass-lang.com/) for styles (using the SCSS syntax)
- [Webpack](https://webpack.github.io/) for compilation

## Requirements

- [Node.js](https://nodejs.org/en/) 6+

```shell
npm install
```

## Running

Make sure to add a `config.js` file in the `config` folder. See the example there for more details.

Production mode:

```shell
npm start
```

Development (Webpack dev server) mode:

```shell
npm run start:dev
```

## Install MonggoDB

add mongoDB repo :https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-centos-7

- sudo vi /etc/yum.repos.d/mongodb-org.repo
- [mongodb-org-4.0]
  name=MongoDB Repository
  baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/4.0/x86_64/
  gpgcheck=1
  enabled=1
  gpgkey=https://www.mongodb.org/static/pgp/server-4.0.asc -
  [ESC] :wq ->write and exit
  - yum repolist - sudo yum install mongodb-org
  - sudo systemctl start mongod
  - sudo systemctl reload mongod
  - sudo systemctl stop mongod
  - sudo tail /var/log/mongodb/mongod.log
  - systemctl is-enabled mongod; echo \$?
